package br.com.itau;

public class Carta {

    private String nipe;
    private String sequencia;

    public String getNipe() {
        return nipe;
    }

    public void setNipe(String nipe) {
        this.nipe = nipe;
    }

    public String getSequencia() {
        return sequencia;
    }

    public void setValor(String sequencia) {
        this.sequencia = sequencia;
    }

    public Carta(String nipe, String sequencia) {
        this.nipe = nipe;
        this.sequencia = sequencia;
    }

}
