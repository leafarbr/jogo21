package br.com.itau;

public class Jogador {

    private String nome;
    private String idade;

    public String getNome() {
        return nome;
    }

    public String getIdade() {
        return idade;
    }

    public Jogador(String nome, String idade) {
        this.nome = nome;
        this.idade = idade;
    }
}
