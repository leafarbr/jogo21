package br.com.itau;

public interface Baralho
{
    public void carregar();
    public void embaralhar();
    public void entregar();

}
