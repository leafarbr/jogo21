package br.com.itau;

public class Carta21 extends Carta{

    private String valor21;

    public String getValor21() {
        return valor21;
    }

    public void setValor21(String valor21) {
        this.valor21 = valor21;
    }

    public Carta21(String nipe, String sequencia, String valor21) {
        super(nipe, sequencia);
        this.valor21 = valor21;
    }

}
