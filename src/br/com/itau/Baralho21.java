package br.com.itau;

import java.util.ArrayList;
import java.util.List;

public class Baralho21 implements Baralho {

    private int quantidade;

    ArrayList<Carta21> cartas = new ArrayList();

    public Baralho21() {
    }

    @Override
    public void carregar() {

        String[] naipes = { "Ouro","Espadas","Copas","Paus" };
        String[] simbolos = {"A","2", "3", "4", "5", "6", "7", "8", "9", "K", "J", "Q"};
        String[] valores = {"1","2", "3", "4", "5", "6", "7", "8", "9", "10", "10", "10"};

        for (int i=0; naipes.length > i; i++) {
            for (int j = 0; simbolos.length > j; j++)
            {
                Carta21 carta = new Carta21(naipes[i], simbolos[j],valores[j]);
                cartas.add(carta);
                quantidade++;
            }
        }

    }

    @Override
    public void embaralhar() {

    }

    @Override
    public void entregar() {

       for (Carta21 carta: cartas) {
            System.out.println("Carta Entregue: " + carta.getNipe());
            System.out.println("Carta Sequência: " + carta.getSequencia());
            System.out.println("Carta Valor: " + carta.getValor21());
       }

    }
}
