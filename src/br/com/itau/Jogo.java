package br.com.itau;

public class Jogo {

    Baralho21 baralho = new Baralho21();

    public Jogo()
    { }

    public void iniciar()
    {
        System.out.println("/////////////////JOGO 21///////////////////");
        System.out.println("//INFORME SEU NOME:");
        System.out.println("//VAMOS JOGAR...");
        baralho.carregar();
        baralho.entregar();

    }

    public void finalizar()
    {
        System.out.println("////////////JOGO ENCERRADO ///////////////////");
    }

    public void exibirhistorico()
    {

        System.out.println("/////////////HISTÓRICO////////////////////////");

    }
}
